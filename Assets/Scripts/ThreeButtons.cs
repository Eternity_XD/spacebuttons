using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ThreeButtons : MonoBehaviour
{
    [SerializeField] private TMP_Text _textUnderMainMenu;
    [SerializeField] private Button _firstButton;
    [SerializeField] private Button _secondButton;
    [SerializeField] private Button _thirdButton;
    [SerializeField] private GameObject _exitButton;


    public void FirstButton()
    {
        _textUnderMainMenu.text = "One clicked";
    }

    public void SecondButton()
    {
        _textUnderMainMenu.text = "Two clicked";
    }

    public void ThirdButton()
    {
        _firstButton.interactable = false;
        _secondButton.interactable = false;
        _thirdButton.interactable = false;
    }

    

    
}
