using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScriptsToManipulateAllPanels : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenuPanel;
    [SerializeField] private GameObject _neededPanelToOpen;
    [SerializeField] private GameObject _returnButton;
    [SerializeField] private TMP_Text _textUnderMainMenu;
    [SerializeField] private TMP_Text _menuText;
    
    public void ButtonPanelClicked()
    {
        _mainMenuPanel.SetActive(false);
        _neededPanelToOpen.SetActive(true);
        _returnButton.SetActive(true);
        _textUnderMainMenu.gameObject.SetActive(true);
        _textUnderMainMenu.text = "Buttons";
        _menuText.text = "Buttons";
    }

    public void TogglePanelClicked()
    {
        _mainMenuPanel.SetActive(false);
        _neededPanelToOpen.SetActive(true);
        _returnButton.SetActive(true);
        _textUnderMainMenu.gameObject.SetActive(true);
        _textUnderMainMenu.text = "Toggles";
        _menuText.text = "Toggles";
    }

    public void DropsPanelClicked()
    {
        _mainMenuPanel.SetActive(false);
        _neededPanelToOpen.SetActive(true);
        _returnButton.SetActive(true);
        _textUnderMainMenu.gameObject.SetActive(true);
        _textUnderMainMenu.text = "Drops";
        _menuText.text = "Drops";
    }

    public void InputPanelClicked()
    {
        _mainMenuPanel.SetActive(false);
        _neededPanelToOpen.SetActive(true);
        _returnButton.SetActive(true);
        _textUnderMainMenu.gameObject.SetActive(true);
        _menuText.text = "Input";
    }

    public void ScrollViewPanelClicked()
    {
        _mainMenuPanel.SetActive(false);
        _neededPanelToOpen.SetActive(true);
        _returnButton.SetActive(true);
        _textUnderMainMenu.gameObject.SetActive(true);
        _textUnderMainMenu.text = "ScrollView";
        _menuText.text = "ScrollView";
    }
}
