using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TogglesValueChanges : MonoBehaviour
{
    [SerializeField] private Toggle _firstToggle;
    [SerializeField] private Toggle _secondToggle;
    [SerializeField] private Toggle _thirdToggle;
    [SerializeField] private TMP_Text _textUnderMainMenu;

    public void FirstToggle()
    {
        _textUnderMainMenu.text = "First";
    }

    public void SecondToggle()
    {
        _textUnderMainMenu.text = "Second";
    }

    public void ThirdToggle()
    {
        _textUnderMainMenu.text = "Third";
    }
}
