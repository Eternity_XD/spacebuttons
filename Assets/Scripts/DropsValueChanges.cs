using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DropsValueChanges : MonoBehaviour
{
    [SerializeField] private TMP_Text _textUnderMainMenu;
    [SerializeField] private TMP_Dropdown _dropDown;

    public void DropDownValueChanged(int index)
    {
        _textUnderMainMenu.text = _dropDown.options[index].text;
    }

    
}
