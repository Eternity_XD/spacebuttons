using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ReturnButton : MonoBehaviour
{
    [SerializeField] private GameObject[] _panels;
    [SerializeField] private TMP_Text _textUnderMainMenu;
    [SerializeField] private TMP_Text _menuText;
    [SerializeField] private Button[] _threButtondForButtonsPanel;

    public void OnClick()
    {
        foreach (var panel in _panels)
        {
            panel.SetActive(false);
        }
        _panels[0].SetActive(true);

        gameObject.SetActive(false);

        _textUnderMainMenu.text = string.Empty;

        _menuText.text = "Main Menu";
    }

    public void EnablingThreeButtons()
    {
        foreach (var button in _threButtondForButtonsPanel)
        {
            button.interactable = true;
        }
    }
}
